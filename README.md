<!-- BEGIN_TF_DOCS -->

# A Terraform project to create management groups, assign subscriptions and manage policies

This project demonstrates how to assign built policies to a hierachy of management groups and also contains an example of a custom policy.

## Usage

This project was created as an alternative to [https://github.com/Azure/terraform-azurerm-caf-enterprise-scale](https://github.com/Azure/terraform-azurerm-caf-enterprise-scale). The CAF solution is not very intuitive and rather difficult to interpret.  This project has adopted the pieces of the code base most required to be effective in a client setting.

This project uses additional modules to:

* Build hierarchy of managment groups as specified in `locals.wec.mg.tf`

```terraform
# Set management Group Deployment Variables
# Root Managament Group is automatically created.
# and assigned the ID specified in the root_id local variable
locals {
  provider_path = {
    management_groups = "/providers/Microsoft.Management/managementGroups/"
    role_assignment   = "/providers/Microsoft.Authorization/roleAssignments/"
  }
  resource_types = {
    policy_definition     = "Microsoft.Authorization/policyDefinitions"
    policy_set_definition = "Microsoft.Authorization/policySetDefinitions"
  }

  management_groups = {
    # Example definition of Parent MG
    "${local.root_id}-root" = {
      display_name               = ("root")
      parent_management_group_id = local.root_id
      subscription_ids           = local.root_subscriptions
      archetype                  = "parent"
    }
    # Example definition of Child MG
    "${local.root_id}-child" = {
      display_name               = ("root-child")
      parent_management_group_id = "${local.root_id}-child"
      subscription_ids           = local.childMG_subscriptions
      archetype                  = "child"
    }
  }
}
```

* Accept list of subscriptions for assignment in `management-groups.tf`

```terraform
data "azurerm_client_config" "current" {}

locals {
  root_id                           = var.ROOT_ID
  root_display_name                 = var.ROOT_DISPLAY_NAME
  rootMG_subscriptions              = var.ROOT_MG_SUBSCRIPTIONS
  childMG_subscriptions             = var.CHILD_MG_SUBSCRIPTIONS
}

# Deploy the Management Group module
module "management_groups" {
  source = "./modules/terraform-azurerm-management-group"

  root_parent_id = data.azurerm_client_config.current.tenant_id
  root_id        = local.root_id
  root_name      = local.root_display_name
  landing_zones  = local.management_groups
}
```

* Assigns policies in `mg-policy-assignment.tf`

```terraform
module "management_group_policy_assignment_root" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  # which archetype this assignment is a member of
  archetype            = "root"
  # where to apply this assignment
  management_group     = "corp"
  # files in this directory must match the policy_assignments section of archetype configs
  assignment_file_path = "${path.module}/lib/policyAssignments"
  # define archetypes of management groups and match to name of policyAssignment .json files
  archetype_files_path = "${path.module}/lib/types"
}

module "management_group_policy_assignment_child" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  archetype            = "child"
  management_group     = "corp-child"
  assignment_file_path = "${path.module}/lib/policyAssignments"
  archetype_files_path = "${path.module}/lib/types"
}
```

Contains example of custom policy definition in `policy-definitions.tf.sav`

```terraform
# Find and read the file data into local Variables.
locals {
  policy_files = fileset("./data", "*.json")
  raw_data     = [for f in local.policy_files : jsondecode(file("./data/${f}"))]
}

/*
 'for' expression is used to convert the Tuple (from local.json_data), to an Object type.
 In depth explanation of 'for' expression can be found in the Readme
*/


module "custom_policy" {
  for_each = { for f in local.raw_data : f.name => f }
  source   = "./modules/terraform-azurerm-policy-definition"

  policy_name      = each.key
  policy_mode      = each.value.properties.mode
  display_name     = each.value.properties.displayName
  management_group = "root"
  metadata         = jsonencode("${each.value.properties.metadata}")   #format("<<METADATA \n %s \n METADATA", each.value.properties.metadata)
  parameters       = jsonencode("${each.value.properties.parameters}") #format("<<PARAMETERS \n %s \n PARAMETERS", each.value.properties.parameters)
  policy_rule      = jsonencode("${each.value.properties.policyRule}") #format("<<POLICYRULE \n %s \n POLICYRULE", each.value.properties.policyRule)
}
```

Contains example of custom policy definition in `policyset-definitions.tf.sav`

```terraform
# Find and read the file data into local Variables.
locals {
  policy_files = fileset("./data", "*.json")
  raw_data     = [for f in local.policy_files : jsondecode(file("./data/${f}"))]
}

module "custom_set" {
  for_each = { for f in local.raw_data : f.name => f }
  source   = "../../"

  policy_set = {
    name             = each.key
    display_name     = each.value.properties.displayName
    description      = each.value.properties.description
    management_group = "root"
  }

  metadata          = jsonencode("${each.value.properties.metadata}")
  parameters        = jsonencode("${each.value.properties.parameters}")
  
  policy_definition = { for p in each.value.properties.policyDefinitions : p.policyDefinitionReferenceId => p }

}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.78.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_management_group_policy_assignment_centralservices"></a> [management\_group\_policy\_assignment\_centralservices](#module\_management\_group\_policy\_assignment\_centralservices) | ./modules/terraform-azurerm-management-group-policy-assignment | n/a |
| <a name="module_management_group_policy_assignment_landingzones"></a> [management\_group\_policy\_assignment\_landingzones](#module\_management\_group\_policy\_assignment\_landingzones) | ./modules/terraform-azurerm-management-group-policy-assignment | n/a |
| <a name="module_management_group_policy_assignment_root"></a> [management\_group\_policy\_assignment\_root](#module\_management\_group\_policy\_assignment\_root) | ./modules/terraform-azurerm-management-group-policy-assignment | n/a |
| <a name="module_management_groups"></a> [management\_groups](#module\_management\_groups) | ./modules/terraform-azurerm-management-group | n/a |

## Resources

| Name | Type |
|------|------|
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/client_config) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_CENTRALSERVICES_MG_SUBSCRIPTIONS"></a> [CENTRALSERVICES\_MG\_SUBSCRIPTIONS](#input\_CENTRALSERVICES\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_DECOMMISSIONED_MG_SUBSCRIPTIONS"></a> [DECOMMISSIONED\_MG\_SUBSCRIPTIONS](#input\_DECOMMISSIONED\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_IDENTITY_MG_SUBSCRIPTIONS"></a> [IDENTITY\_MG\_SUBSCRIPTIONS](#input\_IDENTITY\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_LANDINGZONES_MG_SUBSCRIPTIONS"></a> [LANDINGZONES\_MG\_SUBSCRIPTIONS](#input\_LANDINGZONES\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_MANAGEMENT_MG_SUBSCRIPTIONS"></a> [MANAGEMENT\_MG\_SUBSCRIPTIONS](#input\_MANAGEMENT\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_NETWORK_MG_SUBSCRIPTIONS"></a> [NETWORK\_MG\_SUBSCRIPTIONS](#input\_NETWORK\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_NONPRODUCTION_MG_SUBSCRIPTIONS"></a> [NONPRODUCTION\_MG\_SUBSCRIPTIONS](#input\_NONPRODUCTION\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_PRODUCTION_MG_SUBSCRIPTIONS"></a> [PRODUCTION\_MG\_SUBSCRIPTIONS](#input\_PRODUCTION\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |
| <a name="input_ROOT_DISPLAY_NAME"></a> [ROOT\_DISPLAY\_NAME](#input\_ROOT\_DISPLAY\_NAME) | n/a | `string` | n/a | yes |
| <a name="input_ROOT_ID"></a> [ROOT\_ID](#input\_ROOT\_ID) | n/a | `string` | n/a | yes |
| <a name="input_SANDBOX_MG_SUBSCRIPTIONS"></a> [SANDBOX\_MG\_SUBSCRIPTIONS](#input\_SANDBOX\_MG\_SUBSCRIPTIONS) | n/a | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_m_id"></a> [m\_id](#output\_m\_id) | n/a |
<!-- END_TF_DOCS -->