# Deploy the policy assignment to root management group

module "management_group_policy_assignment_root" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  # which archetype this assignment is a member of
  archetype            = "root"
  # where to apply this assignment
  management_group     = "corp"
  # files in this directory must match the policy_assignments section of archetype configs
  assignment_file_path = "${path.module}/lib/policyAssignments"
  # define archetypes of management groups and match to name of policyAssignment .json files
  archetype_files_path = "${path.module}/lib/types"
}

module "management_group_policy_assignment_centralservices" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  archetype            = "centralservices"
  management_group     = "corp-centralservices"
  assignment_file_path = "${path.module}/lib/policyAssignments"
  archetype_files_path = "${path.module}/lib/types"
}

module "management_group_policy_assignment_landingzones" {
  source = "./modules/terraform-azurerm-management-group-policy-assignment"

  archetype            = "landingzones"
  management_group     = "corp-landingzones"
  assignment_file_path = "${path.module}/lib/policyAssignments"
  archetype_files_path = "${path.module}/lib/types"
}