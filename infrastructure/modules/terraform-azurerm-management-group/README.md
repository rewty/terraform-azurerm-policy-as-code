<!-- BEGIN_TF_DOCS -->

# A simplified version of the CAF Enterprise Scale repo to create a hierarchy of management groups that accept subscription assignments

## Usage

Define the management group structure
```terraform
# Set management Group Deployment Variables
# Root Managament Group is automatically created.
# and assigned the ID specified in the root_id local variable
locals {
  provider_path = {
    management_groups = "/providers/Microsoft.Management/managementGroups/"
    role_assignment   = "/providers/Microsoft.Authorization/roleAssignments/"
  }
  resource_types = {
    policy_definition     = "Microsoft.Authorization/policyDefinitions"
    policy_set_definition = "Microsoft.Authorization/policySetDefinitions"
  }

  management_groups = {
    # Example definition of Parent MG
    "${local.root_id}-root" = {
      display_name               = ("root")
      parent_management_group_id = local.root_id
      subscription_ids           = local.root_subscriptions
      archetype                  = "parent"
    }
    # Example definition of Child MG
    "${local.root_id}-child" = {
      display_name               = ("root-child")
      parent_management_group_id = "${local.root_id}-child"
      subscription_ids           = local.childMG_subscriptions
      archetype                  = "child"
    }
  }
}
```
Deploy the management group structure and assign subscriptions

```terraform
data "azurerm_client_config" "current" {}

locals {
  root_id                           = var.ROOT_ID
  root_display_name                 = var.ROOT_DISPLAY_NAME
  rootMG_subscriptions              = var.ROOT_MG_SUBSCRIPTIONS
  childMG_subscriptions             = var.CHILD_MG_SUBSCRIPTIONS
}

# Deploy the Management Group module
module "management_groups" {
  source = "./modules/terraform-azurerm-management-group"

  root_parent_id = data.azurerm_client_config.current.tenant_id
  root_id        = local.root_id
  root_name      = local.root_display_name
  landing_zones  = local.management_groups
}
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 2.46.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 2.46.1 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_management_group.level_1](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [azurerm_management_group.level_2](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [azurerm_management_group.level_3](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [azurerm_management_group.level_4](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [azurerm_management_group.level_5](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [azurerm_management_group.level_6](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/management_group) | resource |
| [time_sleep.after_azurerm_management_group](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_duration_delay"></a> [create\_duration\_delay](#input\_create\_duration\_delay) | Used to tune terraform apply when faced with errors caused by API caching or eventual consistency. Sets a custom delay period after creation of the specified resource type. | `map(string)` | <pre>{<br>  "azurerm_management_group": "40s",<br>  "azurerm_policy_assignment": "40s",<br>  "azurerm_policy_definition": "40s",<br>  "azurerm_policy_set_definition": "40s",<br>  "azurerm_role_assignment": "10s",<br>  "azurerm_role_definition": "70s"<br>}</pre> | no |
| <a name="input_default_location"></a> [default\_location](#input\_default\_location) | If specified, will use set the default location used for resource deployments where needed. | `string` | `"eastus2"` | no |
| <a name="input_destroy_duration_delay"></a> [destroy\_duration\_delay](#input\_destroy\_duration\_delay) | Used to tune terraform deploy when faced with errors caused by API caching or eventual consistency. Sets a custom delay period after destruction of the specified resource type. | `map(string)` | <pre>{<br>  "azurerm_management_group": "0s",<br>  "azurerm_policy_assignment": "0s",<br>  "azurerm_policy_definition": "0s",<br>  "azurerm_policy_set_definition": "0s",<br>  "azurerm_role_assignment": "0s",<br>  "azurerm_role_definition": "0s"<br>}</pre> | no |
| <a name="input_landing_zones"></a> [landing\_zones](#input\_landing\_zones) | This will deploy additional Management Groups beneath the root Management Group. | `any` | `{}` | no |
| <a name="input_library_path"></a> [library\_path](#input\_library\_path) | If specified, sets the path to a custom library folder for archetype artefacts. | `string` | `""` | no |
| <a name="input_root_id"></a> [root\_id](#input\_root\_id) | If specified, will set a custom Name (ID) value for the "root" Management Group | `string` | `"corp"` | no |
| <a name="input_root_name"></a> [root\_name](#input\_root\_name) | If specified, will set a custom Display Name value for the "root" Management Group. | `string` | `"corp-root"` | no |
| <a name="input_root_parent_id"></a> [root\_parent\_id](#input\_root\_parent\_id) | The root\_parent\_id is used to specify where to set the root for all Landing Zone deployments. Usually the Tenant ID | `string` | n/a | yes |
| <a name="input_root_subscriptionId"></a> [root\_subscriptionId](#input\_root\_subscriptionId) | n/a | `list(string)` | `[]` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | If specified, will set the default tags for all resources deployed by this module where supported. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_management_groups"></a> [management\_groups](#output\_management\_groups) | n/a |
<!-- END_TF_DOCS -->