data "azurerm_client_config" "current" {}

locals {
  root_id                           = var.ROOT_ID
  root_display_name                 = var.ROOT_DISPLAY_NAME
  centralservicesMG_subscriptions   = var.CENTRALSERVICES_MG_SUBSCRIPTIONS
  identityMG_subscriptions          = var.IDENTITY_MG_SUBSCRIPTIONS
  managementMG_subscriptions        = var.MANAGEMENT_MG_SUBSCRIPTIONS
  networkMG_subscriptions           = var.NETWORK_MG_SUBSCRIPTIONS
  landingzonesMG_subscriptions      = var.LANDINGZONES_MG_SUBSCRIPTIONS
  productionMG_subscriptions        = var.PRODUCTION_MG_SUBSCRIPTIONS
  nonproductionMG_subscriptions     = var.NONPRODUCTION_MG_SUBSCRIPTIONS
  sandboxMG_subscriptions           = var.SANDBOX_MG_SUBSCRIPTIONS
  decommissionedMG_subscriptions    = var.DECOMMISSIONED_MG_SUBSCRIPTIONS
}


# Deploy the Management Group module
module "management_groups" {
  source = "./modules/terraform-azurerm-management-group"

  root_parent_id = data.azurerm_client_config.current.tenant_id
  root_id        = local.root_id
  root_name      = local.root_display_name
  landing_zones  = local.management_groups
}

