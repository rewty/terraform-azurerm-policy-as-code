variable "ROOT_ID" {
  type = string
}

variable "ROOT_DISPLAY_NAME" {
  type = string
}

variable "CENTRALSERVICES_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "IDENTITY_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "MANAGEMENT_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "NETWORK_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "LANDINGZONES_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "PRODUCTION_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "NONPRODUCTION_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "SANDBOX_MG_SUBSCRIPTIONS" {
  type    = list(string)
}

variable "DECOMMISSIONED_MG_SUBSCRIPTIONS" {
  type    = list(string)
}