# Set management Group Deployment Variables
# Root Managament Group is automatically created.
# and assigned the ID specified in the root_id local variable
locals {
  provider_path = {
    management_groups = "/providers/Microsoft.Management/managementGroups/"
    role_assignment   = "/providers/Microsoft.Authorization/roleAssignments/"
  }
  resource_types = {
    policy_definition     = "Microsoft.Authorization/policyDefinitions"
    policy_set_definition = "Microsoft.Authorization/policySetDefinitions"
  }

  management_groups = {
    # Central Services - Parent
    "${local.root_id}-centralservices" = {
      display_name               = ("Central Services")
      parent_management_group_id = local.root_id
      subscription_ids           = local.centralservicesMG_subscriptions
      archetype                  = "platform"
    }
    # Central Services - Child - Identity
    "${local.root_id}-identity" = {
      display_name               = ("Identity")
      parent_management_group_id = "${local.root_id}-landingzones"
      subscription_ids           = local.identityMG_subscriptions
      archetype                  = "platform"
    }
    # Central Services - Child - Management
    "${local.root_id}-management" = {
      display_name               = ("Management")
      parent_management_group_id = "${local.root_id}-landingzones"
      subscription_ids           = local.managementMG_subscriptions
      archetype                  = "platform"
    }
    # Central Services - Child - Network
    "${local.root_id}-network" = {
      display_name               = ("Network")
      parent_management_group_id = "${local.root_id}-landingzones"
      subscription_ids           = local.networkMG_subscriptions
      archetype                  = "platform"
    }
    # Landing Zones - Parent
    "${local.root_id}-landingzones" = {
      display_name               = ("Landing Zones")
      parent_management_group_id = local.root_id
      subscription_ids           = local.landingzonesMG_subscriptions
      archetype                  = "landingzone"
    }
    # Landing Zones - Child - Production
    "${local.root_id}-production" = {
      display_name               = ("Production")
      parent_management_group_id = "${local.root_id}-landingzones"
      subscription_ids           = local.productionMG_subscriptions
      archetype                  = "landingzone"
    }
    # Landing Zones - Child - Non Production
    "${local.root_id}-non-prod" = {
      display_name               = ("Non-Production")
      parent_management_group_id = "${local.root_id}-landingzones"
      subscription_ids           = local.nonproductionMG_subscriptions
      archetype                  = "landingzone"
    }
    # Sandbox - Parent
    "${local.root_id}-sandbox" = {
      display_name               = ("Sandbox")
      parent_management_group_id = local.root_id
      subscription_ids           = local.sandboxMG_subscriptions
      archetype                  = "dev"
    }
    # Decommissioned - Parent
    "${local.root_id}-decommissioned" = {
      display_name               = ("Decommissioned")
      parent_management_group_id = local.root_id
      subscription_ids           = local.decommissionedMG_subscriptions
      archetype                  = "decommissioned"
    }
  }
}